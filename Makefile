all::


themes/blackburn/README.md:
	cd themes; git clone https://github.com/yoshiharuyamashita/blackburn.git


all::
	hugo


.PHONY:: publish
publish:
	hugo
	sitecopy -u home
